void setup()// gets called when the program runs
{
  size(1920, 1080, P3D);// changes the size of the popup program when run
  //x = width/2;
  //y = height/2;
  
  camera(0,0,-(height/2)/tan(PI*30/180),
  0,0,0,
  0,-1,0);//copy and paste this every project; moves origin to the middle
  
  //background(130);//grey background only applies for 1 frame
  //grey background but if it is in void draw every frame
  
  background(255);//sets background to white
}


Walker walker = new Walker();

void draw()//gets called every frame
{
  //background(130);
  
  float number = random(8);//can generate float rater than int from visual studio; random(4) is 0-4; also 4 cannot be generated so we have to change it to 5
  println(number);//next line
  
  walker.render();//circle in the middle
  walker.walk();
}
