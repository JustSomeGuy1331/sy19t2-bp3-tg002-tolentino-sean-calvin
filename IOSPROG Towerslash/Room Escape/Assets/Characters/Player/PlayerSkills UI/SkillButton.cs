﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;

public class SkillButton : MonoBehaviour
{

    private Skills skills;

    private void Awake()
    {
        transform.Find("Shoot").GetComponent<Button_UI>().ClickFunc = () =>
        {
            if (!skills.tryUnlockSkill(Skills.Skilltype.Shoot))
            {
                Debug.Log("Cannot Unlock Shoot");
            }
        };

        transform.Find("Bribe").GetComponent<Button_UI>().ClickFunc = () =>
        {
            if(!skills.tryUnlockSkill(Skills.Skilltype.Bribe))
            {
                Debug.Log("Cannot Unlock Bribe"); }
        };

        transform.Find("Dash").GetComponent<Button_UI>().ClickFunc = () =>
        {
            if (!skills.tryUnlockSkill(Skills.Skilltype.Dash))
            { 
                Debug.Log("Cannot Unlock Dash");
            }
        };

        transform.Find("Sabotage").GetComponent<Button_UI>().ClickFunc = () =>
        {
            if (!skills.tryUnlockSkill(Skills.Skilltype.Sabotage))
            {
                Debug.Log("Cannot Unlock Sabotage");
            }
        };
    }

    public void SetPlayerSkills(Skills skills)
    {
        this.skills = skills;
    }
}
