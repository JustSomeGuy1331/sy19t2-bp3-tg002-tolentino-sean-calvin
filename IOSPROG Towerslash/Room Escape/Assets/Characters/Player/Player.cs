﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Player : MonoBehaviour
{
    public float moveSpeed = 3f;
    public Rigidbody2D rb;
    public Animator animator;
    Vector2 movement;

    public static int money = 1000;

    public GameObject player;
    public GameObject EnemyAi;
    public GameObject EnemyAi2;
    public GameObject EnemyAi3;

    private Skills skills;

    private PlayerAbilities abilities;

    private void Awake()
    {
        skills = new Skills();
    }

    void start()
    {
        rb = GetComponent<Rigidbody2D>();
        MoneyUI.Amount += money;
    }
    void Update()
    {
        //input

        movement.x = Input.GetAxisRaw("Horizontal");
        animator.SetFloat("Movementspeed", Mathf.Abs(movement.x));


        movement.y = Input.GetAxisRaw("Vertical");
        animator.SetFloat("Movementspeed", Mathf.Abs(movement.y));

        rb.velocity = new Vector2(movement.x * moveSpeed, movement.y * moveSpeed);
        //rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        GetComponent<PlayerAbilities>().shoot();
        GetComponent<PlayerAbilities>().Bribe();
        GetComponent<PlayerAbilities>().dash();
        GetComponent<PlayerAbilities>().sabotage();

    }

    public Skills GetSkills()
    {
        return skills;
    }

    public bool CanUseShoot()
    {
        return skills.isSkillUnlocked(Skills.Skilltype.Shoot);
    }

    public bool CanUseBribe()
    {
        return skills.isSkillUnlocked(Skills.Skilltype.Bribe);
    }

    public bool CanUseDash()
    {
        return skills.isSkillUnlocked(Skills.Skilltype.Dash);
    }

    public bool CanUseSabotage()
    {
        return skills.isSkillUnlocked(Skills.Skilltype.Sabotage);
    }
}
