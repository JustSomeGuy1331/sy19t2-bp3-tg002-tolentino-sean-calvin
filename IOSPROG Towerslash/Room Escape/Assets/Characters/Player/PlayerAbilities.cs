﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAbilities : MonoBehaviour
{

    public GameObject player;
    public Rigidbody2D rb;

    public float dashSpeed;
    public float startDashTime;
    private float dashTime;
    private int direction;

    public float m_cooldownTimeAB1;
   // public float m_cooldownTimeAB2;
    public float m_cooldownTimeAB3;
    public float m_cooldownTimeAB4;

    private float m_nextFireTimeAB1 = 0;
    //private float m_nextFireTimeAB2 = 0;
    private float m_nextFireTimeAB3 = 0;
    private float m_nextFireTimeAB4 = 0;

    float currentTimeBribe = 0;

    public Bullet ProjectilePreFab;
    public Transform FireOffSet;

    private Skills skills;
    private Player character;


    // Start is called before the first frame update
    void Start()
    {
        dashTime = startDashTime;
        currentTimeBribe = 0;
    }

   public  void dash()
   {
      if(GetComponent<Player>().CanUseDash())
        {
            if (direction == 0)
            {
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    if (Input.GetKeyDown(KeyCode.W))
                    {

                        direction = 1;
                    }
                    else if (Input.GetKeyDown(KeyCode.A))
                    {
                        direction = 2;
                    }
                    else if (Input.GetKeyDown(KeyCode.S))
                    {
                        direction = 3;
                    }
                    else if (Input.GetKeyDown(KeyCode.D))
                    {
                        direction = 4;
                    }
                }
            }
            else
            {
                if (dashTime <= 0)
                {
                    direction = 0;
                    dashTime = startDashTime;
                    rb.velocity = Vector2.zero;
                }
                else
                {
                    dashTime -= Time.deltaTime;

                    if (direction == 1)
                    {
                        rb.velocity = Vector2.up * dashSpeed;
                    }
                    else if (direction == 2)
                    {
                        rb.velocity = Vector2.left * dashSpeed;
                    }
                    else if (direction == 3)
                    {
                        rb.velocity = Vector2.down * dashSpeed;
                    }
                    else if (direction == 4)
                    {
                        rb.velocity = Vector2.right * dashSpeed;
                    }
                }
            }
        }
   }

   public void Bribe()
    {
        if(GetComponent<Player>().CanUseBribe())
        {
            if (Time.time > m_nextFireTimeAB3)
            {
                currentTimeBribe += 1.0f * Time.deltaTime;
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    Debug.Log("BRIBING!!!!");
                    MoneyUI.Amount -= 300;
                    Player.money -= 300;
                    Debug.Log("Cooldown Started");
                    m_nextFireTimeAB3 = Time.time + m_cooldownTimeAB3;
                    player.tag = "Stealth";

                    currentTimeBribe = 0;
                }
                if (currentTimeBribe >= 3)
                {
                    player.tag = "Player";
                }
            }
        }
    }

  public  void sabotage()
  {
     if(GetComponent<Player>().CanUseSabotage())
     {
            if (Time.time > m_nextFireTimeAB4)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Debug.Log("SABOTAGING!!!!");
                    Debug.Log("Cooldown Started");
                    m_nextFireTimeAB4 = Time.time + m_cooldownTimeAB4;

                    player.tag = "Stealth";
                    currentTimeBribe = -3;
                }
                if (currentTimeBribe >= 3)
                {
                    player.tag = "Player";
                }
            }
     }
  }

  public  void shoot()
  {
        if(GetComponent<Player>().CanUseShoot())
        {
            if (Time.time > m_nextFireTimeAB1)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Instantiate(ProjectilePreFab, FireOffSet.position, transform.rotation);
                    Debug.Log("Shooting");
                    Debug.Log("Cooldown Started");
                    m_nextFireTimeAB1 = Time.time + m_cooldownTimeAB1;
                }
            }
        }
  }
}
