﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject gameOverpanel;
    Enemy enemy;

    MenuManager playerSetup;

    public Transform playerSpawn;
    public Transform enemySpawn1;
    public Transform enemySpawn2;
    public Transform powerupSpawn;
    public Transform powerupSpawn2;

    public GameObject enemyPrefab;
    public GameObject playerSpeedPrefab;
    public GameObject playerTankPreFab;
    public GameObject powerUpPrefab;
    public GameObject powerUpPrefab2;

    public TextMeshProUGUI scoreText;
    public int score = 0;

    public TextMeshProUGUI healthText;
    //public int health = 0;

    public GameObject newEnemy;
    public GameObject newEnemy2;
    public GameObject newPickUp;
    public GameObject newPickUp2;

    public float spawnTimer;//add time
    public float spawnRate = 2;//every x seconds to enemy spawn

    public bool isEnemy1Dead;
    public bool isEnemy2Dead;

    private void Start()
    {
        playerSetup = MenuManager.Instance;

        if(playerSetup.playerClass == "Speed")
        {
            Debug.Log("Spawning speed");
            GameObject newPlayer = Instantiate(playerSpeedPrefab, playerSpawn.transform.position, playerSpawn.transform.rotation);
            Camera.main.gameObject.GetComponent<CameraScript>().target = newPlayer.transform;
            //spawn speed player
        }
        else if(playerSetup.playerClass == "Tank")
        {
            Debug.Log("Spawning tank");
            GameObject newPlayer = Instantiate(playerTankPreFab, playerSpawn.transform.position, playerSpawn.transform.rotation);
            Camera.main.gameObject.GetComponent<CameraScript>().target = newPlayer.transform;
            //spawn tank player
        }

        //spawns enemy and pickup at the start
        newEnemy = Instantiate(enemyPrefab, enemySpawn1.transform.position, enemySpawn1.transform.rotation);
        newPickUp = Instantiate(powerUpPrefab, powerupSpawn.transform.position, powerupSpawn.transform.rotation);
        newPickUp2 = Instantiate(powerUpPrefab2, powerupSpawn2.transform.position, powerupSpawn2.transform.rotation);

        scoreText.text = "Score: " + score.ToString();
        healthText.text = "Health: " + GameObject.FindObjectOfType<Player>().health.ToString();
        //healthText.text = "Health: " + health.ToString();
    }

    private void Update()
    {
        spawnTimer += 1 * Time.deltaTime;
        // Debug.Log("Time: " + spawnTimer);

        //Debug.Log("spawn number: " + spawnTimer);
        if (spawnTimer > spawnRate)
        {
            spawnTimer = 0;//anything in update is a loop this is so that it would spawn every x seconds

            //newEnemy = Instantiate(enemyPrefab, enemySpawn1.transform.position, enemySpawn1.transform.rotation);
            //newPickUp = Instantiate(powerUpPrefab, powerupSpawn.transform.position, powerupSpawn.transform.rotation);

            if (isEnemy1Dead == true && GameObject.FindObjectOfType<Player>().Tile2 == true)//if enemy dies spawn the 2nd enemy then move the spawner of the 1st enemy
            {
                Debug.Log("enemy spawns");
                newEnemy2 = Instantiate(enemyPrefab, enemySpawn2.transform.position, enemySpawn2.transform.rotation);
                newPickUp2 = Instantiate(powerUpPrefab2, powerupSpawn2.transform.position, powerupSpawn2.transform.rotation);
                isEnemy1Dead = false;
                
            }
            else if (isEnemy2Dead == true && GameObject.FindObjectOfType<Player>().Tile1 == true)//if enemy dies automatically spawn new enemy
            {
                Debug.Log("enemy is dead");
                newEnemy = Instantiate(enemyPrefab, enemySpawn1.transform.position, enemySpawn1.transform.rotation);
                newPickUp = Instantiate(powerUpPrefab, powerupSpawn.transform.position, powerupSpawn.transform.rotation);
                //newPickUp2 = Instantiate(powerUpPrefab2, powerupSpawn2.transform.position, powerupSpawn2.transform.rotation);
                isEnemy2Dead = false;
                
            }
        }      

        scoreText.text = "Score : " + score.ToString();
        //healthText.text = "Health: " + healthText.ToString();
        healthText.text = "Health: " + GameObject.FindObjectOfType<Player>().health.ToString();
    }
    private void FixedUpdate()
    {
        if (GameObject.FindObjectOfType<Player>().isPlayerDead == true)
        {
            gameOverpanel.SetActive(true);
        }
    }

    public void onRetryButtonClicked()
    {
        SceneManager.LoadScene("GameScene");
    }

    

    
}
