﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeControls : MonoBehaviour
{
    private Vector2 startTouchposition;
    private Vector2 currentPosition;
    private Vector2 endTouchPosition;
    private bool stopTouch = false;

    public float swipeRange;
    public float tapRange;

    // Update is called once per frame
    void Update()
    {
        Swipe();
    }

    public void Swipe()
    {
        
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouchposition = Input.GetTouch(0).position;
        }

        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            currentPosition = Input.GetTouch(0).position;
            Vector2 Distance = currentPosition - startTouchposition;

            if(!stopTouch)
            {
                if(Distance.x < -swipeRange)
                {
                    Debug.Log("Left");
                    stopTouch = true;
                }
                else if(Distance.x > swipeRange)
                {
                    Debug.Log("Right");
                    stopTouch = true;
                }
                else if(Distance.y > swipeRange)
                {
                    Debug.Log("Up");
                    stopTouch = true;
                }
                else if(Distance.y < -swipeRange)
                {
                    Debug.Log("Down");
                    stopTouch = true;
                }
            }
        }

        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            stopTouch = false;

            endTouchPosition = Input.GetTouch(0).position;

            Vector2 Distance = endTouchPosition - startTouchposition;

            if(Mathf.Abs(Distance.x) < tapRange && Mathf.Abs(Distance.y) < tapRange)
            {
                Debug.Log("Tap");
            }
        }

    }


}
