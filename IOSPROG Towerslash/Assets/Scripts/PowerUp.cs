﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    //References
    Player player;

    //public int InvinsibilityPoints;

    //public int healthPoints;
    public void OnCollisionEnter2D(Collision2D collision)
    {
        /*if (collision.gameObject.CompareTag("Player"))
        {
            //GameObject.FindObjectOfType<Player>().InviPoints = InvinsibilityPoints;
            Destroy(gameObject);
            //Debug.Log(GameObject.FindObjectOfType<Player>().InviPoints);

            if(GameObject.FindObjectOfType<Player>().InviPoints > InvinsibilityPoints)
            {
                GameObject.FindObjectOfType<Player>().InviPoints = GameObject.FindObjectOfType<Player>().InviPoints + 1;
                Debug.Log("current invi: " + GameObject.FindObjectOfType<Player>().InviPoints);
            }

        }*/

        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject.FindObjectOfType<Player>().health = GameObject.FindObjectOfType<Player>().health + 1;
            Destroy(gameObject);
            Debug.Log(GameObject.FindObjectOfType<Player>().health);

            //if (GameObject.FindObjectOfType<Player>().health >= healthPoints)
            //{
                GameObject.FindObjectOfType<Player>().health = GameObject.FindObjectOfType<Player>().health;
                Debug.Log("current health: " + GameObject.FindObjectOfType<Player>().health);
            //}

        }
    }

}
