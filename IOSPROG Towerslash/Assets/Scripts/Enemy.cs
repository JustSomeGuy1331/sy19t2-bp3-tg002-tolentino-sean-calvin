﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float speedX = 0;//speed of enemy Sprite 1
    public float speedY = 0;
    public float stuff;
    
    public int pointValue = 0;    
    public int totalPointsCollected;

    //randomize numbers 
    public int number;
    Player playerScript;
    public void RandomGenerate()
    {
        number = Random.Range(1, 4);
    } 

    public Transform arrowSpawn;
    public GameObject[] Arrows;
    // GameObject[] Enemies;

    public void takeDamage(int dNum)
    {
        if (GameObject.FindObjectOfType<Player>().Tile1 == true)
        {
            if (number == dNum)
            {
                GameObject.FindObjectOfType<GameManager>().score += 10;
                GameObject.FindObjectOfType<GameManager>().isEnemy1Dead = true;
                Destroy(GameObject.FindObjectOfType<GameManager>().newEnemy);
            }
        }
        else if (GameObject.FindObjectOfType<Player>().Tile2 == true)
        {
            if (number == dNum)
            {
                GameObject.FindObjectOfType<GameManager>().score += 10;
                GameObject.FindObjectOfType<GameManager>().isEnemy2Dead = true;
                Destroy(GameObject.FindObjectOfType<GameManager>().newEnemy2);
            }
        }

    }

    public void Start()
    {
        RandomGenerate();
        Debug.Log(number);
        Instantiate(Arrows[number - 1], arrowSpawn.transform.position, arrowSpawn.transform.rotation);
        // GameObject.FindObjectOfType<Player>().activeEnemy = this;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))//look over the tag to determine which game object is hit
        {
            if (GameObject.FindObjectOfType<Player>().health <= 0)
            {
                GameObject.FindObjectOfType<Player>().isPlayerDead = true;
                Destroy(this);
            }
            else if (GameObject.FindObjectOfType<Player>().InviPoints == 0)//player is not immune 
            {
                //collision.gameObject.GetComponent<Player>().DoDamage(1);
                GameObject.FindObjectOfType<Player>().health = GameObject.FindObjectOfType<Player>().health - 1;
                GameObject.FindObjectOfType<Player>().InviPoints = 0;

                Debug.Log("Player damaged");
                Debug.Log("current health: " + GameObject.FindObjectOfType<Player>().InviPoints);//player is immune for 1 enemy and kills enemy automatically upon colliding
            }
            else if (GameObject.FindObjectOfType<Player>().InviPoints == 1)
            {

                GameObject.FindObjectOfType<Player>().InviPoints = 0;
                Destroy(this.gameObject);//destroys enemy
                Debug.Log("Enemy dead!");
            }                 
        }
    }
    /*public void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))//look over the tag to determine which game object is hit
        {
            if (GameObject.FindObjectOfType<Player>().InviPoints == 0)//player is not immune 
            {
                //collision.gameObject.GetComponent<Player>().DoDamage(1);
                GameObject.FindObjectOfType<Player>().health = GameObject.FindObjectOfType<Player>().health - 1;
                GameObject.FindObjectOfType<Player>().InviPoints = 0;

                Debug.Log("Player damaged");
                Debug.Log("current health: " + GameObject.FindObjectOfType<Player>().InviPoints);//player is immune for 1 enemy and kills enemy automatically upon colliding
            }
        }
    }*/
    //for some reason the player will not die if I use OnCollisionStay
}
