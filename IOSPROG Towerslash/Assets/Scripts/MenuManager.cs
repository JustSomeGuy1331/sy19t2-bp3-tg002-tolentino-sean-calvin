﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject CharacterSelectPanel;
    public GameObject MainMenuPanel;
    public string playerClass;

    //singleton
    public static MenuManager Instance { get; private set; }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Start()
    {
        MainMenuPanel.SetActive(true);
        CharacterSelectPanel.SetActive(false);
    }
    public void OnPlayButtonClicked()
    {
        MainMenuPanel.SetActive(false);
        CharacterSelectPanel.SetActive(true);
    }

    public void OnBackButtonClicked()
    {
        CharacterSelectPanel.SetActive(false);
        MainMenuPanel.SetActive(true);
    }

    public void OnSpeedPlayerClicked()
    {
        playerClass = "Speed";
        Debug.Log(playerClass);
        SceneManager.LoadScene("GameScene");
    }

    public void OnTankPlayerClicked()
    {
        playerClass = "Tank";
        Debug.Log(playerClass);
        SceneManager.LoadScene("GameScene");
    }
}
