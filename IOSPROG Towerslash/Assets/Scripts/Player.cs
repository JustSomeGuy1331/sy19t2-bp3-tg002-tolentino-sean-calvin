﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Player : MonoBehaviour
{
    public float moveSpeed = 6;

    private Vector2 startTouchposition;
    private Vector2 currentPosition;
    private Vector2 endTouchPosition;
    private bool stopTouch = false;

    public float swipeRange;
    public float tapRange;


    public float dashSpeed;
    public float startDashTime;
    private float dashTime;

    public Rigidbody2D rb;

    public int swipeNumber = 0;//determines what arrow directiondid we swipe towards

    public bool isDashActive;
    public bool isPlayerDead;
    public bool Tile1;
    public bool Tile2;

    public float health;
   
    public int InviPoints = 0;
    public int healthUp = 1;

   
    public void Start()
    {
        dashTime = startDashTime;
        isDashActive = false;
        isPlayerDead = false;

    }

    public void Update()
    {
        Swipe();
        transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        //Debug.Log("" + GameObject.FindObjectOfType<GameManager>().healthText);

    }

    public void DoDamage(int p_health)
    {
        if(health >= 1 && InviPoints == 0)
        {
            Debug.Log("current health health text: " + GameObject.FindObjectOfType<GameManager>().healthText.ToString());
            Debug.Log("current health: " + health);
        }
        else if (health >= 1 && InviPoints == 1)
        {
            health = health - 1;
            Debug.Log("current health: " + GameObject.FindObjectOfType<GameManager>().healthText.ToString());
            Debug.Log("current health: " + health);//+ is print or cout
        }
        else if (health <= 0)
        {
            Destroy(gameObject);//enemy destroyed
            Debug.Log("Deads");
        }    
    }

    public void Swipe()//touch controls
    {

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouchposition = Input.GetTouch(0).position;
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            currentPosition = Input.GetTouch(0).position;
            Vector2 Distance = currentPosition - startTouchposition;

            if (!stopTouch)
            {
                if (Distance.x < -swipeRange)
                {
                    Debug.Log("Left");
                    swipeNumber = 1;
                    GameObject.FindObjectOfType<Enemy>().takeDamage(swipeNumber);
                    stopTouch = true;
                }
                else if (Distance.x > swipeRange)
                {
                    Debug.Log("Right");
                    swipeNumber = 2;
                    GameObject.FindObjectOfType<Enemy>().takeDamage(swipeNumber);
                    stopTouch = true;
                }
                else if (Distance.y > swipeRange)
                {
                    Debug.Log("Up");
                    swipeNumber = 3;
                    GameObject.FindObjectOfType<Enemy>().takeDamage(swipeNumber);
                    stopTouch = true;
                }
                else if (Distance.y < -swipeRange)
                {
                    Debug.Log("Down");
                    swipeNumber = 4;
                    GameObject.FindObjectOfType<Enemy>().takeDamage(swipeNumber);
                    stopTouch = true;
                }

            }
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            stopTouch = false;

            endTouchPosition = Input.GetTouch(0).position;

            Vector2 Distance = endTouchPosition - startTouchposition;

            if (Mathf.Abs(Distance.x) < tapRange && Mathf.Abs(Distance.y) < tapRange)
            {
                Debug.Log("Tap");//player dash only stops when the player taps the screen again and upon doing so gains 20 points

                if (dashTime <= 0)
                {
                    dashTime = startDashTime;
                    rb.velocity = Vector2.zero;
                }
                else
                {
                    dashTime -= Time.time;

                    rb.velocity = Vector2.up * dashSpeed;
                    isDashActive = true;
                }
                isDashActive = false;
                GameObject.FindObjectOfType<GameManager>().score = GameObject.FindObjectOfType<GameManager>().score + 20;
            }
        }

    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Tile1"))
        {
            Tile1 = true;
            Tile2 = false;
            Debug.Log("Tile1 active");
        }
        else if (col.gameObject.CompareTag("Tile2"))
        {
            Tile1 = false;
            Tile2 = true;
            Debug.Log("Tile2 active");
        }
    }

   
}
