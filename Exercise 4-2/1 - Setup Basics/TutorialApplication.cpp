/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

bool TutorialApplication::keyReleased(const OIS::KeyEvent &arg)
{
	BaseApplication::keyReleased(arg);
	speed = 10;
	return true;
}

bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	
	//Ex 3-1: Move Cube
	//Override the frameStarted function and make the cube move using IJKL movement.Note that WASD will not work because it is the default control for navigating tha camera.

	//3-2: Diagonal Movement
	//Make sure to enable diagonal movement on the cube. if I press I it should move forward and when I press I and L it should move diagonally forward to the right.

	//note do not use else if because it would lock the key or button you are pressing for example you press I and while your finger is on I(forward) your othe finger press L(Right) but it only moves I(forward)
	if (mKeyboard->isKeyDown(OIS::KC_L))//moving right
	{
		cube->translate(speed * evt.timeSinceLastFrame, 0, 0);//x,y,z	
		speed = speed + acceleration;
	}
	if (mKeyboard->isKeyDown(OIS::KC_J))//moving left
	{
		cube->translate(-speed * evt.timeSinceLastFrame, 0, 0);//x,y,z
		speed = speed + acceleration;
	}
	if (mKeyboard->isKeyDown(OIS::KC_I))//moving forward
	{
		cube->translate(0, 0, speed * evt.timeSinceLastFrame);//x,y,z
		speed = speed + acceleration;
	}
	if (mKeyboard->isKeyDown(OIS::KC_K))//moving backward
	{
		cube->translate(0, 0, -speed * evt.timeSinceLastFrame);//x,y,z
		speed = speed + acceleration;
	}
	//Ex 4-1: Rotate the Cube
	//Rotate the cube around the x and y axis using NUMPAD8 & 2 for x and NUMPAD4 & 6 for y.Make sure rotation is frame independent.

	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4))//rotate towards the right side or y AXIS
	{
		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		cube->rotate(Vector3(0, 1, 0), Radian(rotation));
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6))//rotate towards the left side or y AXIS
	{
		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		cube->rotate(Vector3(0, -1, 0), Radian(rotation));
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8))//rotate towards the up side or x AXIS
	{
		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		cube->rotate(Vector3(1, 0, 0), Radian(rotation));
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2))//rotate towards the down side or x AXIS
	{
		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		cube->rotate(Vector3(-1, 0, 0), Radian(rotation));
	}
	
	Degree revolution = Degree(45 * evt.timeSinceLastFrame);

	float NewX = (cube->getPosition().x * Math::Cos(Radian(revolution))) + (cube->getPosition().z * Math::Sin(Radian(revolution)));
	float NewZ = (cube->getPosition().x * -Math::Sin(Radian(revolution))) + (cube->getPosition().z * Math::Cos(Radian(revolution)));
	
	cube->setPosition(NewX, 0, NewZ);

	return true;
}

ManualObject* TutorialApplication::createCube(float size)
{
	//ManualObject *manual = mSceneMgr->createManualObject();
	ManualObject *manual = mSceneMgr->createManualObject("manual");

	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	
	//Front verts or vertex
	manual->position(-(size / 2), -(size / 2), (size / 2));
	manual->colour(ColourValue::Blue);
	manual->position((size / 2), -(size / 2), (size / 2));
	manual->colour(ColourValue::Blue);
	manual->position((size / 2), (size / 2), (size / 2));
	manual->colour(ColourValue::Blue);
	manual->position(-(size / 2), (size / 2), (size / 2));
	manual->colour(ColourValue::Blue);

	//Back verts or vertex
	manual->position(-(size / 2), -(size / 2), -(size / 2));
	manual->colour(ColourValue::Red);
	manual->position((size / 2), -(size / 2), -(size / 2));
	manual->colour(ColourValue::Red);
	manual->position((size / 2), (size / 2), -(size / 2));
	manual->colour(ColourValue::Red);
	manual->position(-(size / 2), (size / 2), -(size / 2));
	manual->colour(ColourValue::Red);

	//---------------------------------------------------------------------------
	//Day 2 part 2 of 2 start
	//Front
	manual->position(-10, 10, 10);//index 0
	manual->position(-10, -10, 10);//1
	manual->position(10, -10, 10);//2
	manual->position(10, 10, 10);//3
								 //Back
	manual->position(-10, 10, -10);//index 4
	manual->position(-10, -10, -10);//5
	manual->position(10, -10, -10);//6
	manual->position(10, 10, -10);//7


    //front upper half triangle
	manual->index(0);
	manual->index(1);
	manual->index(3);

	//front lower half triangle
	manual->index(1);
	manual->index(2);
	manual->index(3);

	//back upper half triangle
	manual->index(5);
	manual->index(4);
	manual->index(7);

	//back lower half triangle
	manual->index(7);
	manual->index(6);
	manual->index(5);

	//right lower half triangle
	manual->index(3);
	manual->index(2);
	manual->index(6);

	//right upper half triangle
	manual->index(6);
	manual->index(7);
	manual->index(3);

	//left lower half triangle
	manual->index(0);
	manual->index(5);
	manual->index(1);

	//left upper half triangle
	manual->index(5);
	manual->index(0);
	manual->index(4);

	//top lower half triangle
	manual->index(3);
	manual->index(7);
	manual->index(4);

	//top upper half triangle
	manual->index(4);
	manual->index(0);
	manual->index(3);

	//bottom lower half triangle
	manual->index(2);
	manual->index(1);
	manual->index(5);

	//bottom upper half triangle
	manual->index(5);
	manual->index(6);
	manual->index(2);

	//End Drawing
	manual->end();

	//Add the object to the scene
	//mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(triangle);

	return manual;
	//Day 2 part 2 of 2 end
	//-------------------------------------------------------------------------------------------------------
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)

	//Day 3 part 1 of 3? start
	ManualObject* manual = createCube(10.0f);//cube becomes smaller the lower the number

	

	cube = mSceneMgr->getRootSceneNode()
		->createChildSceneNode();

	cube->attachObject(manual);
	//-------------------------------------------------------------------------------------------------------

	//Day 4 start part 1
	cube->translate(10, 0, 0);//cube spawns at x = 10, y = 0, and z = 0
	
	//-------------------------------------------------------------------------------------------------------

	//Day 1 start

	//ManualObject *triangle = mSceneMgr->createManualObject();
	//triangle->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
		
	//Day 1 start
	//Setup vertex position
	//triangle->position(10, 10, 10);//front lower half triangle
	//triangle->position(-10, -10, 10);
	//triangle->position(10, -10, 10);

	//triangle->position(-10, -10, 10);//front upper half triangle
	//triangle->position(10, 10, 10);
	//triangle->position(-10, 10, 10);



	//triangle->position(10, 10, 10);//right side lower half triangle
	//
	//triangle->position(10, -10, 10);
	//triangle->position(10, -10, -10);

	//triangle->position(10, -10, -10);//right side upper half triangle
	//
	//triangle->position(10, 10, -10);
	//triangle->position(10, 10, 10);



	//triangle->position(-10, 10, 10);//left side lower half triangle
	//triangle->position(-10, -10, -10);
	//triangle->position(-10, -10, 10);
	//

	//triangle->position(-10, -10, -10);//left side upper half triangle
	//triangle->position(-10, 10, 10);
	//triangle->position(-10, 10, -10);



	//triangle->position(10, 10, -10);//back lower half triangle
	//
	//triangle->position(10, -10, -10);
	//triangle->position(-10, -10, -10);

	//triangle->position(-10, -10, -10);//back upper half triangle
	//
	//triangle->position(-10, 10, -10);
	//triangle->position(10, 10, -10);
	//


	//triangle->position(10, 10, 10);//top lower half triangle
	//
	//triangle->position(10, 10, -10);
	//triangle->position(-10, 10, -10);

	//triangle->position(-10, 10, -10);//top upper half triangle
	//
	//triangle->position(-10, 10, 10);
	//triangle->position(10, 10, 10);



	//triangle->position(10, -10, 10);//bottom lower half triangle
	//triangle->position(-10, -10, 10);
	//triangle->position(-10, -10, -10);
	//

	//triangle->position(-10, -10, -10);//bottom upper half triangle
	//triangle->position(10, -10, -10);
	//triangle->position(10, -10, 10);

	//Day 1 end
	//-------------------------------------------------------------------------------------------------------

	//Day 2 part 1 of 2 start

	//ManualObject *manual = mSceneMgr->createManualObject();

	//manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	////Front
	//manual->position(-10, 10, 10);//index 0
	//manual->position(-10, -10, 10);//1
	//manual->position(10, -10, 10);//2
	//manual->position(10, 10, 10);//3
	////Back
	//manual->position(-10, 10, -10);//index 4
	//manual->position(-10, -10, -10);//5
	//manual->position(10, -10, -10);//6
	//manual->position(10, 10, -10);//7

	//		
	////front upper half triangle
	//manual->index(0);
	//manual->index(1);
	//manual->index(3);

	////front lower half triangle
	//manual->index(1);
	//manual->index(2);
	//manual->index(3);

	////back upper half triangle
	//manual->index(5);
	//manual->index(4);
	//manual->index(7);

	////back lower half triangle
	//manual->index(7);
	//manual->index(6);
	//manual->index(5);

	////right lower half triangle
	//manual->index(3);
	//manual->index(2);
	//manual->index(6);

	////right upper half triangle
	//manual->index(6);
	//manual->index(7);
	//manual->index(3);

	////left lower half triangle
	//manual->index(0);
	//manual->index(5);
	//manual->index(1);

	////left upper half triangle
	//manual->index(5);
	//manual->index(0);
	//manual->index(4);

	////top lower half triangle
	//manual->index(3);
	//manual->index(7);
	//manual->index(4);

	////top upper half triangle
	//manual->index(4);
	//manual->index(0);
	//manual->index(3);

	////bottom lower half triangle
	//manual->index(2);
	//manual->index(1);
	//manual->index(5);

	////bottom upper half triangle
	//manual->index(5);
	//manual->index(6);
	//manual->index(2);

	////End Drawing
	//manual->end();

	//Add the object to the scene
	//mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(triangle);           //propably from day 1

	//ManualObject *manual = createCube(20);

	//mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(manual);
	//Day 2 part 1 of 2 end
	//---------------------------------------------------------------------------
	
	
}
//---------------------------------------------------------------------------


#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
