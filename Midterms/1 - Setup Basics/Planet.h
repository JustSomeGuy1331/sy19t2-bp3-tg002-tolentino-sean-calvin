#pragma once

#include "TutorialApplication.h"
#include <OgreManualObject.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
using namespace Ogre;

class Planet
{
public:
	//	Planet(SceneNode* node);
	//	static Planet* createPlanet(SceneManager* sceneManager, float size, float distance);
	//	~Planet();
	//	void update(const FrameEvent& evt, float revolverDistance, float revolution, Planet* revolve);
	//	void setParent(Planet* parent);
	//	float getX();
	//	float getZ();
	//	SceneNode* getNode();
	//	void setLocalRotationSpeed(float speed);
	//	void setRevolutionSpeed(float speed);
	//	void Planet::setPosition(float x, float y, float z);
	//
	//	SceneNode* mNode;
	//	float* mLocalRotationSpeed;
	//	float* mRevolutionSpeed;
	//	float*mX = 0;
	//	float*mZ = 0;
	Planet(SceneNode* node);
	static Planet* createPlanet(SceneManager* scneManager, float size);
	void rotationRevolution(const FrameEvent& evt, float revolution, Planet* revolver, float rotation);
	SceneNode* getSceneNode();
	Vector3 setLocation(float x);
	Vector3 getPosition();
	SceneNode* mNode;
};



