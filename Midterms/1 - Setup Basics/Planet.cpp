#pragma once
#include "Planet.h"
#include "TutorialApplication.h"
#include <OgreManualObject.h>
//Planet::Planet(SceneNode* node)
//{
//	this->mNode = node;
//	
//}
//
//Planet* Planet::createPlanet(SceneManager* sceneManager, float size, float distance)
//{
//	SceneNode* node = sceneManager->getRootSceneNode()->createChildSceneNode();
//	ManualObject* thing = sceneManager->createManualObject();
//	Planet* planet1 = new Planet(node);
//	thing->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
//	// front
//	thing->position(size / 2, size / 2, size / 2);
//	thing->position(-size / 2, size / 2, size / 2);
//	thing->position(size / 2, -size / 2, size / 2);
//
//	thing->position(-size / 2, size / 2, size / 2);
//	thing->position(-size / 2, -size / 2, size / 2);
//	thing->position(size / 2, -size / 2, size / 2);
//
//
//	//back
//	thing->position(size / 2, size / 2, -size / 2);
//	thing->position(size / 2, -size / 2, -size / 2);
//	thing->position(-size / 2, size / 2, -size / 2);
//
//	thing->position(-size / 2, size / 2, -size / 2);
//	thing->position(size / 2, -size / 2, -size / 2);
//	thing->position(-size / 2, -size / 2, -size / 2);
//
//	//right side
//	thing->position(size / 2, size / 2, -size / 2);
//	thing->position(size / 2, size / 2, size / 2);
//	thing->position(size / 2, -size / 2, -size / 2);
//
//	thing->position(size / 2, -size / 2, -size / 2);
//	thing->position(size / 2, size / 2, size / 2);
//	thing->position(size / 2, -size / 2, size / 2);
//
//
//
//	//left side
//	thing->position(-size / 2, size / 2, -size / 2);
//	thing->position(-size / 2, -size / 2, -size / 2);
//	thing->position(-size / 2, size / 2, size / 2);
//
//	thing->position(-size / 2, -size / 2, -size / 2);
//	thing->position(-size / 2, -size / 2, size / 2);
//	thing->position(-size / 2, size / 2, size / 2);
//
//	//bottom
//	thing->position(size / 2, -size / 2, size / 2);
//	thing->position(-size / 2, -size / 2, size / 2);
//	thing->position(-size / 2, -size / 2, -size / 2);
//
//	thing->position(-size / 2, -size / 2, -size / 2);
//	thing->position(size / 2, -size / 2, -size / 2);
//	thing->position(size / 2, -size / 2, size / 2);
//
//	////top
//	thing->position(size / 2, size / 2, size / 2);
//	thing->position(-size / 2, size / 2, -size / 2);
//	thing->position(-size / 2, size / 2, size / 2);
//
//	thing->position(-size / 2, size / 2, -size / 2);
//	thing->position(size / 2, size / 2, size / 2);
//	thing->position(size / 2, size / 2, -size / 2);
//	
//	node->setPosition(distance, 0, distance);
//	thing->end();
//
//	node->attachObject(thing);
//	//this is a new thing
//	return planet1;
//}
//
//void Planet::update(const FrameEvent& evt, float revolverDistance, float revolution, Planet*revolve)
//{
//	//http://www.djcxy.com/p/95552.html?fbclid=IwAR0dueeLUHmnal_X9ZoM-skqTwn6TcylpYqysacPSjL18OEUYdAF8uXmjrA
//
//	//Vector3 location = Vector3::ZERO;
//	//float distance = revolverDistance;
//	/*location.x = distance, location.z = distance, location.y = 0;
//	mNode->setPosition(location.x, location.y, location.z);*/
//	Degree planetRevolution = Degree(revolution * evt.timeSinceLastFrame);
//	float locationX, locationZ;
//	locationX = getX() - revolve->getX();
//	locationZ = getZ() - revolve->getZ();
//	float oldX = locationX;
//	float oldZ = locationZ;
//	float newX = (oldX * Math::Cos(planetRevolution)) + (oldZ * Math::Sin(planetRevolution));
//	float newZ = (oldX * -Math::Sin(planetRevolution)) + (oldZ * Math::Cos(planetRevolution));
//	setPosition(newX, 0, newZ);
//
//
//
//}
//
//
//
//
//
//
//void Planet::setPosition(float x, float y, float z)
//{
//	this->mNode->setPosition(x, y, z);
//}
//
//SceneNode* Planet::getNode()
//{
//	return mNode;
//}
//
//float Planet::getX()
//{
//	return mX;
//}
//
//float Planet::getZ()
//{
//	return mZ;
//}

Planet::Planet(SceneNode* node)
{
	mNode = node;

}

Planet* Planet::createPlanet(SceneManager* sceneManager, float size)
{


	SceneNode* node = sceneManager->getRootSceneNode()->createChildSceneNode();
	ManualObject* heavenlyBody = sceneManager->createManualObject();

	Planet* planet = new Planet(node);
	heavenlyBody->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//front
	heavenlyBody->position(size / 2, size / 2, size / 2);
	heavenlyBody->position(-size / 2, size / 2, size / 2);
	heavenlyBody->position(size / 2, -size / 2, size / 2);

	heavenlyBody->position(-size / 2, size / 2, size / 2);
	heavenlyBody->position(-size / 2, -size / 2, size / 2);
	heavenlyBody->position(size / 2, -size / 2, size / 2);


	//back
	heavenlyBody->position(size / 2, size / 2, -size / 2);
	heavenlyBody->position(size / 2, -size / 2, -size / 2);
	heavenlyBody->position(-size / 2, size / 2, -size / 2);

	heavenlyBody->position(-size / 2, size / 2, -size / 2);
	heavenlyBody->position(size / 2, -size / 2, -size / 2);
	heavenlyBody->position(-size / 2, -size / 2, -size / 2);

	//right side
	heavenlyBody->position(size / 2, size / 2, -size / 2);
	heavenlyBody->position(size / 2, size / 2, size / 2);
	heavenlyBody->position(size / 2, -size / 2, -size / 2);

	heavenlyBody->position(size / 2, -size / 2, -size / 2);
	heavenlyBody->position(size / 2, size / 2, size / 2);
	heavenlyBody->position(size / 2, -size / 2, size / 2);



	//left side
	heavenlyBody->position(-size / 2, size / 2, -size / 2);
	heavenlyBody->position(-size / 2, -size / 2, -size / 2);
	heavenlyBody->position(-size / 2, size / 2, size / 2);

	heavenlyBody->position(-size / 2, -size / 2, -size / 2);
	heavenlyBody->position(-size / 2, -size / 2, size / 2);
	heavenlyBody->position(-size / 2, size / 2, size / 2);

	//bottom
	heavenlyBody->position(size / 2, -size / 2, size / 2);
	heavenlyBody->position(-size / 2, -size / 2, size / 2);
	heavenlyBody->position(-size / 2, -size / 2, -size / 2);

	heavenlyBody->position(-size / 2, -size / 2, -size / 2);
	heavenlyBody->position(size / 2, -size / 2, -size / 2);
	heavenlyBody->position(size / 2, -size / 2, size / 2);

	////top
	heavenlyBody->position(size / 2, size / 2, size / 2);
	heavenlyBody->position(-size / 2, size / 2, -size / 2);
	heavenlyBody->position(-size / 2, size / 2, size / 2);

	heavenlyBody->position(-size / 2, size / 2, -size / 2);
	heavenlyBody->position(size / 2, size / 2, size / 2);
	heavenlyBody->position(size / 2, size / 2, -size / 2);


	heavenlyBody->end();
	node->attachObject(heavenlyBody);
	return planet;
}

void Planet::rotationRevolution(const FrameEvent& evt, float revolution, Planet* revolver, float rotation)
{

	/*Vector3 location = getSceneNode()->getPosition();*/
	//revolver->planetLocation = getSceneNode()->getPosition();
	Degree planetRevolution = Degree((revolution)* evt.timeSinceLastFrame);
	//minus this location
	float oldX = getPosition().x - revolver->getPosition().x;
	float oldZ = getPosition().z - revolver->getPosition().z;
	//add it back here xD
	float newX = (oldX * Math::Cos(Radian(planetRevolution))) + (oldZ * Math::Sin(Radian(planetRevolution)));
	float newZ = (oldX * -Math::Sin(Radian(planetRevolution))) + (oldZ * Math::Cos(Radian(planetRevolution)));
	newX += revolver->getPosition().x;
	newZ += revolver->getPosition().z;

	getSceneNode()->setPosition(Vector3(newX, revolver->getSceneNode()->getPosition().y, newZ));
	getSceneNode()->rotate(Vector3(0, 1, 0), Radian(rotation));
	/*modifyX(newX);
	modifyZ(newZ);
	getSceneNode()->translate(newX, 0, newZ);
	getSceneNode()->rotate(Vector3(0, 1, 0), Radian(revolution));*/

}




SceneNode* Planet::getSceneNode()
{
	// TODO: insert return statement here
	return mNode;
}

Vector3 Planet::setLocation(float x)
{
	return Vector3(x, 0, 0);
}

Vector3 Planet::getPosition()
{
	return mNode->getPosition();
}



