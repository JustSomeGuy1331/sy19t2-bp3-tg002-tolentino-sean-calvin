/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include "BaseApplication.h"
#include <OgreManualObject.h>

#include <string>

#include <iostream>
#include <string>
#include <time.h>
#include <stdlib.h>

using namespace Ogre;

//---------------------------------------------------------------------------
class Planet;//forward declaring the planet do not use #include "Planet.h" since tutorial.cpp already has it because trying to do so(this is cross-referencing) 
//would lead to Planet* nSun, nEath, etc... having errors;

class TutorialApplication : public BaseApplication
{
public://makes use of everyone even other class
	TutorialApplication(void);
	virtual ~TutorialApplication(void);

protected://can be used by anyone that has a parent-child relationship


	//SceneManager* sceneManager; do not use
	//float node;

	//float speed = 10;
	//float acceleration = 1.2;
	virtual void createScene(void);
	virtual bool frameStarted(const FrameEvent & evt);

	//virtual bool keyPressed(const OIS::KeyEvent &arg);      //unknown use	
	//virtual bool keyReleased(const OIS::KeyEvent &arg);

private://usame class
	//ManualObject * createCube(float size);
	//void createScene(Planet* Planet);
	std::vector<Planet*> planets;
	SceneNode *cube;
	Planet* nSun;
	Planet* nMercury;
	Planet* nVenus;
	Planet* nEarth;
	Planet* nMoon;
	Planet* nMars;
};

//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------
