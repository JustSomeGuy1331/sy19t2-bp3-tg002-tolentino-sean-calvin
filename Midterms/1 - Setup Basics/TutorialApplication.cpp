/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "Planet.h"
#include <vector>

//---------------------------------------------------------------------------

TutorialApplication::TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
void cube(int size, ManualObject* heavenlyBody)
{

	//front
	heavenlyBody->position(size / 2, size / 2, size / 2);
	heavenlyBody->position(-size / 2, size / 2, size / 2);
	heavenlyBody->position(size / 2, -size / 2, size / 2);

	heavenlyBody->position(-size / 2, size / 2, size / 2);
	heavenlyBody->position(-size / 2, -size / 2, size / 2);
	heavenlyBody->position(size / 2, -size / 2, size / 2);


	//back
	heavenlyBody->position(size / 2, size / 2, -size / 2);
	heavenlyBody->position(size / 2, -size / 2, -size / 2);
	heavenlyBody->position(-size / 2, size / 2, -size / 2);

	heavenlyBody->position(-size / 2, size / 2, -size / 2);
	heavenlyBody->position(size / 2, -size / 2, -size / 2);
	heavenlyBody->position(-size / 2, -size / 2, -size / 2);

	//right side
	heavenlyBody->position(size / 2, size / 2, -size / 2);
	heavenlyBody->position(size / 2, size / 2, size / 2);
	heavenlyBody->position(size / 2, -size / 2, -size / 2);

	heavenlyBody->position(size / 2, -size / 2, -size / 2);
	heavenlyBody->position(size / 2, size / 2, size / 2);
	heavenlyBody->position(size / 2, -size / 2, size / 2);



	//left side
	heavenlyBody->position(-size / 2, size / 2, -size / 2);
	heavenlyBody->position(-size / 2, -size / 2, -size / 2);
	heavenlyBody->position(-size / 2, size / 2, size / 2);

	heavenlyBody->position(-size / 2, -size / 2, -size / 2);
	heavenlyBody->position(-size / 2, -size / 2, size / 2);
	heavenlyBody->position(-size / 2, size / 2, size / 2);

	//bottom
	heavenlyBody->position(size / 2, -size / 2, size / 2);
	heavenlyBody->position(-size / 2, -size / 2, size / 2);
	heavenlyBody->position(-size / 2, -size / 2, -size / 2);

	heavenlyBody->position(-size / 2, -size / 2, -size / 2);
	heavenlyBody->position(size / 2, -size / 2, -size / 2);
	heavenlyBody->position(size / 2, -size / 2, size / 2);

	////top
	heavenlyBody->position(size / 2, size / 2, size / 2);
	heavenlyBody->position(-size / 2, size / 2, -size / 2);
	heavenlyBody->position(-size / 2, size / 2, size / 2);

	heavenlyBody->position(-size / 2, size / 2, -size / 2);
	heavenlyBody->position(size / 2, size / 2, size / 2);
	heavenlyBody->position(size / 2, size / 2, -size / 2);


}
void TutorialApplication::createScene(void)
{
	// Create your scene here :)

	 /*Planets*/
	nSun = Planet::createPlanet(mSceneMgr, 20);
	nMercury = Planet::createPlanet(mSceneMgr, 2);
	nMercury->getSceneNode()->translate(Vector3(40, 0, 0));
	nVenus = Planet::createPlanet(mSceneMgr, 10);
	nVenus->getSceneNode()->translate(Vector3(60, 0, 0));
	nEarth = Planet::createPlanet(mSceneMgr, 10);
	nEarth->getSceneNode()->translate(Vector3(80, 0, 0));
	nMoon = Planet::createPlanet(mSceneMgr, 1);
	nMoon->getSceneNode()->translate(Vector3(100, 0, 0));
	nMars = Planet::createPlanet(mSceneMgr, 10);
	nMars->getSceneNode()->translate(Vector3(120, 0, 0));
	//node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	//ManualObject* triangle = mSceneMgr->createManualObject();
	//triangle->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	//cube(10, triangle);
	//triangle->end();
	//node->attachObject(triangle);
	//






}
//---------------------------------------------------------------------------
bool TutorialApplication::frameStarted(const FrameEvent & evt)
{

	/*Vector3 movement = Vector3::ZERO;*/
	/*float x = 0.1;
	mercury->update(evt, 40, 30, sun);
	*/
	//Vector3 movement = Vector3::ZERO;
	//float x = 10;
	//float y = 10;
	//float acceleration = 0;
	//if (mKeyboard->isKeyDown(OIS::KC_I))
	//{
	//	node->translate(0, 0, (x += evt.timeSinceLastFrame));
	//	if (mKeyboard->isKeyDown(OIS::KC_I))
	//	{
	//		x += acceleration;
	//		acceleration++;
	//	}
	//	else
	//	{
	//		acceleration = 0;
	//	}
	//}
	//else if(mKeyboard->isKeyDown(OIS::KC_K))
	//{
	//	node->translate(0, 0, (x -= evt.timeSinceLastFrame));
	//	if (mKeyboard->isKeyDown(OIS::KC_K))
	//	{
	//		x -= acceleration;
	//		acceleration++;
	//	}
	//	else if (!mKeyboard->isKeyDown(OIS::KC_K))
	//	{
	//		acceleration = 0;
	//	}
	//}
	//if (mKeyboard->isKeyDown(OIS::KC_J))
	//{
	//	node->translate(0, 0, (y += evt.timeSinceLastFrame));
	//	if (mKeyboard->isKeyDown(OIS::KC_J))
	//	{
	//		y += acceleration;
	//		acceleration++;
	//	}
	//	else if (!mKeyboard->isKeyDown(OIS::KC_J))
	//	{
	//		acceleration = 0;
	//	}
	//}
	//else if (mKeyboard->isKeyDown(OIS::KC_L))
	//{
	//	node->translate(0, 0, (y -= evt.timeSinceLastFrame));
	//	if (mKeyboard->isKeyDown(OIS::KC_L))
	//	{
	//		y -= acceleration;
	//		acceleration++;
	//	}
	//	else if (!mKeyboard->isKeyDown(OIS::KC_L))
	//	{
	//		acceleration = 0;
	//	}
	//}
	//movement *= evt.timeSinceLastFrame;
	//
	//if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6))
	//{
	//	Degree rotation = Degree(360 * evt.timeSinceLastFrame);
	//	node->rotate(Vector3(0, 1, 0), Radian(rotation));
	//}

	//Planets
	nMercury->rotationRevolution(evt, 88, nSun, 59);
	nVenus->rotationRevolution(evt, 224.7f, nSun, 43);
	nEarth->rotationRevolution(evt, 365, nSun, 1);
	nMoon->rotationRevolution(evt, 20, nEarth, 0);
	nMars->rotationRevolution(evt, 686.93f, nSun, 1.37f);



	//Vector3 location = Vector3::ZERO;
	//
	//Degree mercuryRevolution = Degree((88/360) * evt.timeSinceLastFrame);
	//location.x = mercury->mDistance - sun->mDistance;
	//location.z = mercury->mDistance - sun->mDistance;
	//location.y = 0;
	//float oldX = location.x;
	//float oldZ = location.z;
	//float newX = (oldX * Math::Cos(mercuryRevolution)) + (oldZ * Math::Sin(mercuryRevolution));
	//float newZ = (oldX * -Math::Sin(mercuryRevolution)) + (oldZ * Math::Cos(mercuryRevolution));
	//mMercury->translate(newX, 0, newZ);
	//mMercury->rotate(Vector3(0, 1, 0), Radian(mercuryRevolution));

	return true;
}
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
