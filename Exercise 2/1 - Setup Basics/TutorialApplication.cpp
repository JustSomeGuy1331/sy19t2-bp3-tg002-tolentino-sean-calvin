/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

ManualObject* TutorialApplication::createCube(float size)
{
	ManualObject *manual = mSceneMgr->createManualObject();
	
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//Front
	manual->position(-10, 10, 10);//index 0
	manual->position(-10, -10, 10);//1
	manual->position(10, -10, 10);//2
	manual->position(10, 10, 10);//3
								 //Back
	manual->position(-10, 10, -10);//index 4
	manual->position(-10, -10, -10);//5
	manual->position(10, -10, -10);//6
	manual->position(10, 10, -10);//7


    //front upper half triangle
	manual->index(0);
	manual->index(1);
	manual->index(3);

	//front lower half triangle
	manual->index(1);
	manual->index(2);
	manual->index(3);

	//back upper half triangle
	manual->index(5);
	manual->index(4);
	manual->index(7);

	//back lower half triangle
	manual->index(7);
	manual->index(6);
	manual->index(5);

	//right lower half triangle
	manual->index(3);
	manual->index(2);
	manual->index(6);

	//right upper half triangle
	manual->index(6);
	manual->index(7);
	manual->index(3);

	//left lower half triangle
	manual->index(0);
	manual->index(5);
	manual->index(1);

	//left upper half triangle
	manual->index(5);
	manual->index(0);
	manual->index(4);

	//top lower half triangle
	manual->index(3);
	manual->index(7);
	manual->index(4);

	//top upper half triangle
	manual->index(4);
	manual->index(0);
	manual->index(3);

	//bottom lower half triangle
	manual->index(2);
	manual->index(1);
	manual->index(5);

	//bottom upper half triangle
	manual->index(5);
	manual->index(6);
	manual->index(2);

	//End Drawing
	manual->end();

	//Add the object to the scene
	//mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(triangle);

	return manual;
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	//ManualObject *triangle = mSceneMgr->createManualObject();
	//triangle->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//Setup vertex position
	//triangle->position(10, 10, 10);//front lower half triangle
	//triangle->position(-10, -10, 10);
	//triangle->position(10, -10, 10);

	//triangle->position(-10, -10, 10);//front upper half triangle
	//triangle->position(10, 10, 10);
	//triangle->position(-10, 10, 10);



	//triangle->position(10, 10, 10);//right side lower half triangle
	//
	//triangle->position(10, -10, 10);
	//triangle->position(10, -10, -10);

	//triangle->position(10, -10, -10);//right side upper half triangle
	//
	//triangle->position(10, 10, -10);
	//triangle->position(10, 10, 10);



	//triangle->position(-10, 10, 10);//left side lower half triangle
	//triangle->position(-10, -10, -10);
	//triangle->position(-10, -10, 10);
	//

	//triangle->position(-10, -10, -10);//left side upper half triangle
	//triangle->position(-10, 10, 10);
	//triangle->position(-10, 10, -10);



	//triangle->position(10, 10, -10);//back lower half triangle
	//
	//triangle->position(10, -10, -10);
	//triangle->position(-10, -10, -10);

	//triangle->position(-10, -10, -10);//back upper half triangle
	//
	//triangle->position(-10, 10, -10);
	//triangle->position(10, 10, -10);
	//


	//triangle->position(10, 10, 10);//top lower half triangle
	//
	//triangle->position(10, 10, -10);
	//triangle->position(-10, 10, -10);

	//triangle->position(-10, 10, -10);//top upper half triangle
	//
	//triangle->position(-10, 10, 10);
	//triangle->position(10, 10, 10);



	//triangle->position(10, -10, 10);//bottom lower half triangle
	//triangle->position(-10, -10, 10);
	//triangle->position(-10, -10, -10);
	//

	//triangle->position(-10, -10, -10);//bottom upper half triangle
	//triangle->position(10, -10, -10);
	//triangle->position(10, -10, 10);

	//ManualObject *manual = mSceneMgr->createManualObject();

	//manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	////Front
	//manual->position(-10, 10, 10);//index 0
	//manual->position(-10, -10, 10);//1
	//manual->position(10, -10, 10);//2
	//manual->position(10, 10, 10);//3
	////Back
	//manual->position(-10, 10, -10);//index 4
	//manual->position(-10, -10, -10);//5
	//manual->position(10, -10, -10);//6
	//manual->position(10, 10, -10);//7

	//		
	////front upper half triangle
	//manual->index(0);
	//manual->index(1);
	//manual->index(3);

	////front lower half triangle
	//manual->index(1);
	//manual->index(2);
	//manual->index(3);

	////back upper half triangle
	//manual->index(5);
	//manual->index(4);
	//manual->index(7);

	////back lower half triangle
	//manual->index(7);
	//manual->index(6);
	//manual->index(5);

	////right lower half triangle
	//manual->index(3);
	//manual->index(2);
	//manual->index(6);

	////right upper half triangle
	//manual->index(6);
	//manual->index(7);
	//manual->index(3);

	////left lower half triangle
	//manual->index(0);
	//manual->index(5);
	//manual->index(1);

	////left upper half triangle
	//manual->index(5);
	//manual->index(0);
	//manual->index(4);

	////top lower half triangle
	//manual->index(3);
	//manual->index(7);
	//manual->index(4);

	////top upper half triangle
	//manual->index(4);
	//manual->index(0);
	//manual->index(3);

	////bottom lower half triangle
	//manual->index(2);
	//manual->index(1);
	//manual->index(5);

	////bottom upper half triangle
	//manual->index(5);
	//manual->index(6);
	//manual->index(2);

	////End Drawing
	//manual->end();

	//Add the object to the scene
	//mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(triangle);

	ManualObject *manual = createCube(20);
	

	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(manual);
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
